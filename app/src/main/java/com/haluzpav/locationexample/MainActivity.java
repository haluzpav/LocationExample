package com.haluzpav.locationexample;

import android.app.Activity;
import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends Activity {

    LocationManager locationManager;
    LocationListener locationListener;
    GpsStatus gpsStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepareViews();
        prepareLocation();
    }

    /**
     * Stops receiving location so it doesn't drain battery
     */
    @Override
    protected void onStop() {
        super.onStop();
        Switch gettingSwitch = (Switch) findViewById(R.id.switch1);
        if (gettingSwitch.isChecked()) {
            //stopGettingLocation();
        }
    }

    /**
     * Resumes receiving location
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        Switch gettingSwitch = (Switch) findViewById(R.id.switch1);
        if (gettingSwitch.isChecked()) {
            startGettingLocation(getSelectedProvider());
        }
    }

    /**
     * Initializes views and its listeners
     */
    private void prepareViews() {
        Spinner providerSpinner = (Spinner) findViewById(R.id.spinner);
        providerSpinner.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        String[] providers = {LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER};
        ArrayAdapter providerSpinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, providers);
        providerSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        providerSpinner.setAdapter(providerSpinnerAdapter);

        Switch gettingSwitch = (Switch) findViewById(R.id.switch1);
        gettingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startGettingLocation(getSelectedProvider());
                } else {
                    stopGettingLocation();
                }
            }
        });

        Button getLastButton = (Button) findViewById(R.id.button);
        getLastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printLocation(getLastLocation(getSelectedProvider()));
            }
        });
    }

    /**
     * Initializes LocationManager and LocationListener(s)
     */
    private void prepareLocation() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            int count = 0;

            @Override
            public void onLocationChanged(Location location) {
                printCount(count++);
                printLocation(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                String string = "provider " + provider + " became ";
                switch (status) {
                    case LocationProvider.AVAILABLE:
                        string += "available";
                    case LocationProvider.OUT_OF_SERVICE:
                        string += "out of service";
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        string += "temporarily unavailable";
                }
                printProviderStatus(string);
            }

            @Override
            public void onProviderEnabled(String provider) {
                printProviderStatus("provider " + provider + " is enabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                printProviderStatus("provider " + provider + " is disabled");
            }
        };

        gpsStatus = null;
        GpsStatus.Listener gpsStatusListener = new GpsStatus.Listener() {
            @Override
            public void onGpsStatusChanged(int event) {
                printGpsStatus(event, locationManager.getGpsStatus(gpsStatus));
            }
        };
        locationManager.addGpsStatusListener(gpsStatusListener);

        /*
        // Gives empty nmea strings on my phone
        GpsStatus.NmeaListener nmeaListener = new GpsStatus.NmeaListener() {
            @Override
            public void onNmeaReceived(long timestamp, String nmea) {
                System.out.println(nmea + " " + timestamp);
            }
        };
        locationManager.addNmeaListener(nmeaListener);
        */
    }

    private String getSelectedProvider() {
        Spinner providerSpinner = (Spinner) findViewById(R.id.spinner);
        return providerSpinner.getSelectedItem().toString();
    }

    private void startGettingLocation(String provider) {
        locationManager.requestLocationUpdates(provider, 0, 0, locationListener);
    }

    private void stopGettingLocation() {
        locationManager.removeUpdates(locationListener);
    }

    /**
     * Receives last usable (based on time and maybe etc.) location from provider
     * @param provider used provider
     * @return last location
     */
    private Location getLastLocation(String provider) {
        return locationManager.getLastKnownLocation(provider);
    }

    private void printCount(int count) {
        TextView textView = (TextView) findViewById(R.id.count);
        textView.setText("Count: " + count);
    }

    private void printLocation(Location location) {
        String string = "Location not known";
        if (location != null) {
            string = "Time of fix: " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")).format(new Date(location.getTime())) +
                    "\nLatitude: " + location.getLatitude() +
                    "\nLongitude: " + location.getLongitude() +
                    "\nAltitude: " + location.getAltitude() +
                    "\nAccuracy: " + location.getAccuracy() +
                    "\nSpeed: " + location.getSpeed()
            ;
        }
        TextView textView = (TextView) findViewById(R.id.location);
        textView.setText(string);
    }

    private void printGpsStatus(int event, GpsStatus gpsStatus) {
        TextView textView = (TextView) findViewById(R.id.gpsStatus);
        String string = "Undefined event";
        switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                string = "GPS system has started.";
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                string = "GPS system has stopped.";
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                string = "Got first fix";
                TextView firstFix = (TextView) findViewById(R.id.gpsStatusFirstFix);
                firstFix.setText("Time for first fix: " + gpsStatus.getTimeToFirstFix());
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                int available = 0;
                int used = 0;
                for (GpsSatellite gpsSatellite : gpsStatus.getSatellites()) {
                    available++;
                    if (gpsSatellite.usedInFix()) {
                        used++;
                    }
                }
                string = "Max possible satellites: " + gpsStatus.getMaxSatellites() +
                        "\nAvailable satellites: " + available +
                        "\nSatellites used in fix: " + used
                ;
        }
        textView.setText(string);
    }

    private void printProviderStatus(String string) {
        TextView textView = (TextView) findViewById(R.id.providerStatus);
        textView.setText(string);
    }

}
